const openForm = document.getElementById('form-btn-open');
const checkInForm = document.querySelector('.check-input-box');
const closeForm = document.querySelector('#close-btn');

openForm.addEventListener('click', () => {
  checkInForm.style.visibility = 'visible'
})

closeForm.addEventListener('click', () => {
  checkInForm.style.visibility = 'hidden';
  checkInForm.style.height = 0;
})